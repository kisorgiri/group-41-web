// life cycle of components

// based on class based component

// init
// constructor first
// render 2nd
// componentDidMount(); //3rd

// update
// render();
// componentDidUpdate()
// 1st argument for previous Props and 2nd for previous State

// destroy

// componentWillUnmount();

// hooks

// hooks are function that enhance the component
// 16.8.0 
// 17.0.2

// routing ==> react-router-dom
// v6  ==> thirdparty(library specific hooks)

// types of  inbuilt hooks
// basic hooks
// State Hooks ==> state manage 
// Effect Hooks ==> life cycle 
// types ==>
// contextHooks ==> 

// eg <NewsFeed a = "aaa"/>

// news feed ===> <Comment >

// comment ==> <Likes a="props.a">

// advance hooks
// useReducer()
// useMemo()
// useCallback()
// useImperativeHandle()
// useDebugValue()
// useRef()
// useLayoutEffect()

// in general we use use prefix to identify hooks
// hooks cannot be implemented inside class based component
// hooks should be used in higher level of functions
// hooks should not be used inside loops

// state hooks


// FLUX architecture
// 
// views ==> actions ==> dispatcher ==> store 

// unidirectional data flow
// views ==> actions==> dispatchers ==> store

// there can be multiple store in an application
// store handle main logic

// REDUX ==> on top of flux
// concerns
// views ==> react application (UI)
// actions ===> action builder (API call, data preparation)
// reducers ==> plain function which is responsible to update store (logic resides on reducer)
// store ==> container to hold data

// unidirectional data flow
// views --> actions---> reducers ---> store
// single source of truth
// one store in a application


// deployement

// platform ==> heroku

// if serverside
// db must be hosted on cloud ==> 
// atlash service from mongodb

// client side (react application)

// host react app inside express-app(node) to deploy on heroku
