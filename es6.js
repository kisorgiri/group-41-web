// ECMAScript


// ECMA version

// latest

// JS engine 

// ECMAScirpt

// stable version ==> es5

// es6 and beyond

// Object Shorthand
// Object Destruction
// spread operator
// rest operator
// arrow notation function
// default arguments
// template literals
// import and export
// class
// block scope
// 

// Object Shorthand
// let phone = '2999';
// let phone1 = '299sss9';

// const addr = {
//   tempAddr: 'bkt',
//   permanentAddr: 'patan',
//   phone,
//   phone1
// };


// var newObj = {name,addr,phone,email,test,xyz}
// console.log('object shorthand', addr)

// Object Destruction

// function buySomething() {
//   var fruits = 'apple';
//   var chocolates = 'kitkat';
//   var fish = 'fish';

//   return {
//     fruits,
//     chocolates,
//     fish

//   }
// }

// var result = buySomething(); // general flow

// destruction pattern
// var { fruits,fish } = buySomething();



// // console.log('result is >>', result)
// console.log('fruits', fruits)
// // console.log('fish >>', fish)



// function sendMail({email,subject}){
//   // use email and subject
// }


// sendMail({
//   a,
//   b,
//   c,
// })


// spread operator  || rest operator 
// ...

// spread operator 
// spread value of object and array

// var obj = {
//   email: 'test@gmail.com',
//   addr: 'bkt',
//   phone: 3388
// }

// var obj2 = {
//   ...obj,
//   status: 'online',
//   price: 333,
//   phone: 9999999
// };

// obj.phone = 9851016046
// obj2.addr = 'lalitpur'

// console.log('obj >>', obj);
// console.log('obj2', obj2)

// var obj3 = {
//   ...obj,
//   ...obj2
// }

// console.log('final obj', obj3)
var subject = 'forgot password';
var sender = 'ramesh';

function prepareEmail() {
  return {
    sender: 'ram',
    receiver: 'shyam',
    subject: 'Activate your account',
    message: 'how are you'
  }
}


var { subject: random, message, ...rest } = prepareEmail();

console.log('subject >>', subject)
console.log('random >>', random)
console.log('sender >>', sender)
console.log('rest is ', rest)

// arrow notation function

// const welcome = function(){

// }

// // arrow noation

// const welcome = ()=>{

// }

// const goodBye = function(title){

// }

// const goodBye = (title)=>{

// }

// const sum = function (num2, num3) {
//   return num2 + num3
// }

// const sum = (num2, num4) => {
//   return num2 + num4
// }

// one liner function [without middle braces and return keyword]
// const sum = (num1, num3) => num1 + num3;

// // note ==> if we have only one arg placeholder parenthesis() is optional

// const hi = name => 'hello    ' + name;

// var result = sum(2, 5);
// console.log('sum is >', result)

// console.log('hi >   ', hi('bikram'))

let laptops = [{
  name: 'legion',
  brand: 'lenovo',
  price: 333,
  color: 'black'
}, {
  name: 'elitebook',
  brand: 'hp',
  price: 39,
  color: 'black'
}, {
  name: 'macbook pro',
  brand: 'apple',
  price: 44,
  color: 'silver'
}, {
  name: 'random',
  brand: 'random',
  price: 333,
  color: 'red'
}]

// var appleLaptops = laptops.filter(function (item) {
//   if (item.brand === 'apple') {
//     return true;
//   }
// })

// es6

var appleLaptops = laptops.filter(item => item.brand === 'apple')

console.log('apple Laptops >', appleLaptops)

// NOTE:- arrow notation function will inherit parent's this(scope)

// default argument


function sendMailAgain(details = 'hi and welcome') {
  console.log('details >>', details)
  // var subject = details.subject;
  // use subject
}

sendMailAgain();

function changestatus(status = 'active') {

}


// template literals

// var message = hi + name + 'welcome to '+ addr + 'if you
// ldskjf
// ldskjf';
var name = 'broadway';
var addr = 'tinkune';

var text = `hi ${name} and welcome to
${addr}
if you have any concerns
please connect with our support team`
console.log('text is >>', text)

// block scope

// var will maintain functional scope(local scope)
// let will maintain block scope

function welcome(name){
  if(name){
    // scope maintianed with if block is block scope
    // var will not maintain blockscope
    // let will maintain blockscope
  }
}


// import and export

// export
// syntax

// 1. named export
// export class abc
// export const xyz = 'value'

// named export can be multiple in a file


// 2. default export

// export default <any value> array, number, boolean

// note => there can be only one default export

// there can be both multiple named export and one default export

// eg
// named export 

export const welcome = (addr)=>{

}

export class Students {

}

// import
// import totally depends on how it is exported

// if named export

// import {name1,name3,name4} from 'source' [source can be from npmjs modules and own modules]

// if default export
// import randomName from 'source'

// if both'

// import anyName,{name1,name2,name3} from source

// element


// pre-requisities
// html 
// css
// taskrunner 
// compiler
// JSX ==> Javascript extended syntax

// traditional way
// html >>> vitra >>> JS <script>

// JSX pattern
// JS ....>  HTML CODE ...
