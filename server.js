const express = require('express')
const app = express();
const path = require('path');

app.use(express.static(path.join(process.cwd(), 'build')));

app.get('/*', function (req, res, next) {
  res.sendFile(path.join(process.cwd() + 'build/index.html'));
})


app.listen(process.env.PORT || 8080, function (err, done) {
  if (!err) {
    console.log('server listening')
  }
})