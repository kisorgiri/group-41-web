import { notify } from './../utils/notify'
export const handleError = (err) => {
  let errMsg = 'Something Went Wrong';
  debugger;
  let error = err && err.response;
  if (error) {
    let serverErr = error.data;
    if (serverErr && typeof (serverErr.msg) === 'string') {
      errMsg = serverErr.msg;
    }
    if (serverErr && typeof (serverErr.msg) === 'object') {
      // db error message
      // extract error message from db 

      errMsg = extractDbErrMsg(serverErr.msg)
    }
  }
  else {
    let errorFromXHR = JSON.parse(err);
    if (errorFromXHR) {
      errMsg = errorFromXHR.msg;
    }
  }
  // check err
  // parse err
  // prepare err message
  // extract err message
  // show it in UI
  return notify.showError(errMsg)
}


const extractDbErrMsg = dbErr => {
  let keys = Object.keys(dbErr.keyPattern);
  let errMsg;
  switch (dbErr.code) {
    case 11000:
      errMsg = `Provided value ${dbErr.keyValue[keys[0]]} is duplicate for ${keys[0]}`
      break;
    default:
      break;
  }
  return errMsg;
}