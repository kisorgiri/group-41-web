import axios from "axios";
const BaseAPIURL = process.env.REACT_APP_BASE_API_URL;

// base instance
const http = axios.create({
  baseURL: BaseAPIURL,
  timeout: 10000,
  timeoutErrorMessage: 'Error connecting to API',
  responseType: 'json'
})

const getHeaders = (isSecured = false) => {
  let options = {
    'Content-Type': 'application/json'
  }
  if (isSecured) {
    options['Authorization'] = `Bearer ${localStorage.getItem('token')}`
  }

  return options;
}

// eg /auth/login, ?time=now
/**
 * jsdocs style of commenting
 * @param {string} url 
 * @param {object} params 
 * @returns Promise
 */
const GET = (url, isSecured = false, params = {}) => {
  return http.get(url, {
    headers: getHeaders(isSecured),
    params: params
  })
}

const POST = (url, data, isSecured = false, params = {}) => {
  return http.post(url, data, {
    headers: getHeaders(isSecured),
    params: params
  })
}

const PUT = (url, data, isSecured = false, params = {}) => {
  return http.put(url, data, {
    headers: getHeaders(isSecured),
    params: params
  })
}

const DELETE = (url, isSecured = false, params = {}) => {
  return http.delete(url, {
    headers: getHeaders(isSecured),
    params: params
  })
}

const UPLOAD = (method, url, data = {}, files = []) => {
  return new Promise((resolve, reject) => {

    // xhr instance and formdata instance
    const xhr = new XMLHttpRequest();
    const formData = new FormData();

    // append files in formData
    files.forEach((item, index) => {
      formData.append('images', item, item.name)
    })

    // append textual data in formData
    for (let key in data) {
      formData.append(key, data[key])
    }

    xhr.onreadystatechange = () => {
      // ready state value will determine the state of file upload
      // 0==> not initilized
      // 1= initilixed .....
      // 4 => request response cycle complete
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          return resolve(xhr.response)
        } else {
          return reject(xhr.response)
        }
      }
    }

    xhr.open(method, `${BaseAPIURL}${url}?token=Beareer ${localStorage.getItem('token')}`, true);
    xhr.send(formData);

  })
}

export const httpClient = {
  GET,
  POST,
  PUT,
  DELETE,
  UPLOAD
}


