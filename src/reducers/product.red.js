import { ProductActionTypes } from './../actions/product.ac'

export const ProductReducer = (state, action) => {
  console.log('action is >>', action)
  switch (action.type) {
    case ProductActionTypes.SET_IS_LOADING:
      return {
        ...state,
        isLoading: action.payload
      }

    case ProductActionTypes.PRODUCTS_RECEIVED:
      return {
        ...state,
        products: action.payload
      }
    default:
      return {
        ...state
      }
  }
}