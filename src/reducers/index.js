import { combineReducers } from 'redux'
import { ProductReducer } from './product.red'

export const rootReducer = combineReducers({
  products: ProductReducer
})