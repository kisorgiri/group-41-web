// import { configureStore } from 'reduxjs/toolkit';

import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import { rootReducer } from './reducers';

const middlewares = [thunk];

const defaultState = {
  products: {
    isLoading: false,
    products: []
  }
};

export const store = createStore(rootReducer, defaultState, applyMiddleware(...middlewares))

// example 1

// {
//   users: [],
//     nofications;[],
//       products: [],
//         productsIsLoading: false,
//           productSiSubmitting: false,
//             user: { },
//   userIsLoading: false
// }

// example 2
// {
//   user: {
//     isLoading: false,
//       data: [],
//         user: { }
//   },
//   notification: {
//     isLoading: false
//   },
//   products: {
//     isLoading: false
//   }
// }





// revision

// /concerns
// views
// actions
// reducers
// store

// pacakges and library

// redux, react-redux redux thunk


// redux ==> createStore, applyMiddleware, combineReducers

// react-redux ==> providers, connect
// provider will provide store
// connect will connect components with store

// 1st arg mapStateToProps {}
// 2nd arg mapDispatchToProps{}


// reduc thunk =m> middleware which will enable us to dispatch an action with delay
