// action should be dispatched to reducer

import { handleError } from "../services/errorHandler"
import { httpClient } from "../utils/httpClient"

export const ProductActionTypes = {
  SET_IS_LOADING: 'SET_IS_LOADING',
  PRODUCTS_RECEIVED: 'PROUDCTS_RECEIVED'
}

// export const testFn = (params) => {
//   return {
//     type: 'hi',
//     message: 'hello'
//   }
// }

// export const abc = (params) => {
//   return (dispatch) => {
//     // dispatch must be called to pass control in reducer
//   }
// }

export const fetch_product_ac = (params) => (dispatch) => {
  console.log('inside actions')
  dispatch({
    type: ProductActionTypes.SET_IS_LOADING,
    payload: true
  })

  httpClient
    .GET('/products', true)
    .then(({ data }) => {
      dispatch({
        type: ProductActionTypes.PRODUCTS_RECEIVED,
        payload: data
      })
    })
    .catch(err => {
      handleError(err)
    })
    .finally(() => {
      dispatch({
        type: ProductActionTypes.SET_IS_LOADING,
        payload: false
      })
    })
}