import { BrowserRouter, Routes, Route, useParams, useLocation, Navigate } from 'react-router-dom';
import { ForgotPassword } from './auth/forgotPassword/forgotPassword.component';
import { Login } from './auth/login/login.component';
import { Register } from './auth/register/register.component';
import { ResetPassword } from './auth/resetPassword/resetPassword.component';
import { Header } from './common/header/header.component';
import { Sidebar } from './common/sidebar/sidebar.component';
import { AddProduct } from './products/addProduct/addProduct.component';
import { EditProduct } from './products/editProduct/editProduct.component';
import { SearchProduct } from './products/searchProduct/searchProduct.component';
import { ViewProducts } from './products/viewProducts/viewProducts.component';
import { Dashboard } from './users/dashboard/dashboard.component';
import MessageComponent from './users/messages/message.component';

const PageNotFound = (props) => {

  return (
    <div>
      <p>Page Not Found Page</p>
      <img src="./images/notFound.jpg" alt="notFound.jpg" width="400px"></img>

    </div>
  );
}


const PublicRoute = ({ component: Component, ...rest }) => {
  return (
    <div>
      <Header isLoggedIn={localStorage.getItem('token') ? true : false}></Header>
      <div className="main_content">
        <Component {...rest} ></Component>
      </div>
      <Sidebar isLoggedIn={localStorage.getItem('token') ? true : false}></Sidebar>
    </div>
  )
}


const ProtectedRoute = ({ component: Component, ...rest }) => {
  return localStorage.getItem('token')
    ? (
      <div>
        <Header isLoggedIn={true}></Header>
        <div className="main_content">
          <Component {...rest} ></Component>
        </div>
        <Sidebar isLoggedIn={true} />
      </div>
    )
    : <Navigate to="/"></Navigate>
}

export const AppRouting = (props) => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<PublicRoute component={Login}></PublicRoute>} />
        <Route path="/register" element={<PublicRoute component={Register}> </PublicRoute>} />
        <Route path="/forgot_password" element={<PublicRoute component={ForgotPassword} />} />
        <Route path="/reset_password/:id" element={<PublicRoute component={ResetPassword} />} />
        <Route path="/dashboard" element={<ProtectedRoute component={Dashboard}></ProtectedRoute>}></Route>
        <Route path="/add_product" element={<ProtectedRoute component={AddProduct}></ProtectedRoute>}></Route>
        <Route path="/edit_product/:id" element={<ProtectedRoute component={EditProduct}></ProtectedRoute>}></Route>
        <Route path="/view_products" element={<ProtectedRoute component={ViewProducts}></ProtectedRoute>}></Route>
        <Route path="/view_products" element={<ProtectedRoute component={ViewProducts}></ProtectedRoute>}></Route>
        <Route path="/messages" element={<ProtectedRoute component={MessageComponent}></ProtectedRoute>}></Route>
        <Route path="/search_products" element={<PublicRoute component={SearchProduct}></PublicRoute>}></Route>
        <Route path="*" element={<PublicRoute component={PageNotFound} ></PublicRoute>}></Route>

      </Routes>
    </BrowserRouter>
  )
}



// routing 
// library ==> react-router-dom

// BrowserRouter
// >> follwed by Router

// <Route> configuration block

// path and element

// Link ==> to do routing in click event

// hooks 
// useNavigate ==> navigation
// useParams ==> url ma aaune value 
// useLocation ==> optional value extract from url  40


// 404
// useHistory

