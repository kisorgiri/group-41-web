import React from 'react';
import { AppRouting } from './app.routing';
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux'
import { store } from './../store';


// functional component (stateless)
export const App = (args) => {
  // it should return single html node
  return (
    <div>
      <Provider store={store}>
        <AppRouting></AppRouting>
        <ToastContainer />
      </Provider>
    </div>
  )
}

// routing component (configuration)




// component 
// basic building block of react js
// component is responsible for returning a single html node
// component can be written as a html elemnt with custom attribute

// component can be statefull
// component can be stateless

// component can be written in a function
// component can be written in a class

// state || props
// state and props are component's data
// incoming data for components ==> props ==> stateless component

// data within component ==> states ==> statefull component

// version==> 17.02
// 16.8 ==> hooks was introduced

// before 16.8 
// class based component can only maintain state 
// after 16.8
// functional component can also maintain state using hooks

// usimg redux
// everything is passed in a form of props


// without redux
// component vitrai API call garera statema ma data maintain garna sakchau