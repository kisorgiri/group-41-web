import React, { useState, useEffect } from 'react';
import { SubmitButton } from '../../common/submitButton/submitButton.component';
import { handleError } from '../../../services/errorHandler';
import { httpClient } from '../../../utils/httpClient';
import { formatDate } from './../../../utils/index'
import { FaTrashAlt } from 'react-icons/fa'

const IMG_URL = process.env.REACT_APP_IMG_URL;

const defaultForm = {
  category: '',
  name: '',
  brand: '',
  price: '',
  quantity: '',
  color: '',
  size: '',
  description: '',
  description_summary: '',
  modelNo: '',
  purchasedDate: '',
  salesDate: '',
  manuDate: '',
  expiryDate: '',
  slug: '',
  isReturnEligible: '',
  isFeatured: '',
  isIsoCertified: '',
  specifications: '',
  warrentyStatus: '',
  warrentyPeriod: '',
  tags: '',
  discountedItem: '',
  discountType: '',
  discountValue: '',
}

export const ProductForm = (props) => {
  const [categories, setCategories] = useState([]);
  const [filesToUpload, setFilesToUpload] = useState([])
  const [filesToPreview, setFilesToPreview] = useState([])
  const [filesToRemove, setFilesToRemove] = useState([])

  const [formFields, setFormFields] = useState({
    ...defaultForm
  })
  // init
  useEffect(() => {
    // TODO :// investigate memory leakage 
    httpClient.GET('/category', true)
      .then(({ data }) => {
        setCategories(data);
      })
      .catch(err => handleError(err))

    // edit only
    if (props.productData) {
      const { productData } = props;
      setFormFields({
        ...defaultForm,
        ...productData,
        category: productData?.category?._id,
        discountedItem: (productData.discount && productData.discount.discountedItem) ? true : false,
        discountType: (productData.discount && productData.discount.discountType) ? productData.discount.discountType : '',
        discountValue: (productData.discount && productData.discount.discountValue) ? productData.discount.discountValue : '',
        purchasedDate: productData.purchasedDate ? formatDate(productData.purchasedDate, 'YYYY-MM-DD') : '',
        salesDate: productData.salesDate ? formatDate(productData.salesDate, 'YYYY-MM-DD') : '',
        manuDate: productData.manuDate ? formatDate(productData.manuDate, 'YYYY-MM-DD') : '',
        expiryDate: productData.expiryDate ? formatDate(productData.expiryDate, 'YYYY-MM-DD') : ''
      })
      setFilesToPreview(productData.images)
    }
    return () => {
      console.log('clean up')
    }
  }, [props.productData])

  const handleSubmit = e => {
    e.preventDefault();
    const requestData = formFields;
    requestData.filesToRemove = filesToRemove;
    props.submitCallback(requestData, filesToUpload);
  }

  const handleChange = e => {
    let { name, value, type, checked, files } = e.target;
    if (type === 'checkbox') {
      value = checked;
    }
    if (type === 'file') {
      // for multiple
      filesToUpload.push(files[0]);
      setFilesToUpload(filesToUpload)
    }

    setFormFields({
      ...formFields,
      [name]: value
    })
  }
  const removeImageSelection = (index) => {
    filesToUpload.splice(index, 1);
    setFilesToUpload([...filesToUpload])
  }

  const removeExistingImage = (item, index) => {
    // save item information in state
    filesToRemove.push(item);
    setFilesToRemove([...filesToRemove])
    filesToPreview.splice(index, 1);
    setFilesToPreview([...filesToPreview])
  }

  return (
    <>
      <h2>{props.title}</h2>
      <p>{props.description}</p>
      <form noValidate onSubmit={handleSubmit} className="form-group">
        <label>Category</label>
        <select className="form-control" value={formFields.category} onChange={handleChange} name="category">
          <option value="">(Select Category)</option>
          {categories.map((cat) => (
            <option key={cat._id} value={cat._id}> {cat.name}</option>))
          }

        </select>
        <label>Name</label>
        <input type="text" className="form-control" value={formFields.name} onChange={handleChange} placeholder="Name" name="name"></input>
        <label>Description</label>
        <textarea rows={6} className="form-control" value={formFields.description} onChange={handleChange} placeholder="Description" name="description"></textarea>
        <label>Brand</label>
        <input type="text" className="form-control" value={formFields.brand} onChange={handleChange} placeholder="Brand" name="brand"></input>
        <label>Description Summary</label>
        <input type="text" className="form-control" value={formFields.description_summary} onChange={handleChange} placeholder="Description Summary" name="description_summary"></input>
        <label>Color</label>
        <input type="text" className="form-control" value={formFields.color} onChange={handleChange} placeholder="Color" name="color"></input>
        <label>Price</label>
        <input type="number" className="form-control" value={formFields.price} onChange={handleChange} name="price"></input>
        <label>Quantity</label>
        <input type="number" className="form-control" value={formFields.quantity} onChange={handleChange} name="quantity"></input>
        <label>Size</label>
        <input type="text" className="form-control" value={formFields.size} onChange={handleChange} placeholder="Size" name="size"></input>
        <label>Model No.</label>
        <input type="text" className="form-control" value={formFields.modelNo} onChange={handleChange} placeholder="Model No." name="modelNo"></input>
        <label>Purchased Date</label>
        <input type="date" className="form-control" value={formFields.purchasedDate} onChange={handleChange} name="purchasedDate"></input>
        <label>Sales Date</label>
        <input type="date" className="form-control" value={formFields.salesDate} onChange={handleChange} name="salesDate"></input>
        <label>Manu Date</label>
        <input type="date" className="form-control" value={formFields.manuDate} onChange={handleChange} name="manuDate"></input>
        <label>Expiry Date</label>
        <input type="date" className="form-control" value={formFields.expiryDate} onChange={handleChange} name="expiryDate"></input>
        <label>Slug</label>
        <input type="text" className="form-control" value={formFields.slug} onChange={handleChange} placeholder="Slug" name="slug"></input>
        <input type="checkbox" checked={formFields.isReturnEligible} onChange={handleChange} name="isReturnEligible"></input>
        <label> &nbsp;Is Return Eligible</label>
        <br />
        <input type="checkbox" checked={formFields.isFeatured} onChange={handleChange} name="isFeatured"></input>
        <label>&nbsp;Is Featured</label>
        <br />
        <input type="checkbox" checked={formFields.isIsoCertified} onChange={handleChange} name="isIsoCertified"></input>
        <label>&nbsp;Is ISO Certified</label>
        <br />
        <label>Specifications</label>
        <input type="text" className="form-control" value={formFields.specifications} onChange={handleChange} placeholder="Specifications" name="specifications"></input>
        <input type="checkbox" onChange={handleChange} checked={formFields.warrentyStatus} name="warrentyStatus"></input>
        <label> &nbsp;Warrenty Status</label>
        <br />
        <label>Warrenty Period</label>
        <input type="text" className="form-control" value={formFields.warrentyPeriod} onChange={handleChange} placeholder="Warrenty Period" name="warrentyPeriod"></input>
        <label>Tags</label>
        <input type="text" className="form-control" value={formFields.tags} onChange={handleChange} placeholder="Tags" name="tags"></input>
        <input type="checkbox" onChange={handleChange} checked={formFields.discountedItem} name="discountedItem"></input>
        <label> &nbsp;Discounted Item</label>
        <br />
        <label>Discount Type</label>
        <select className="form-control" value={formFields.discountType} onChange={handleChange} name="discountType">
          <option value="">(Select Type)</option>
          <option value="percentage">Percentage</option>
          <option value="quantity">Quantity</option>
          <option value="value">Value</option>
        </select>
        <label>Discount Value</label>
        <input type="text" className="form-control" value={formFields.discountValue} onChange={handleChange} placeholder="Discount Value" name="discountValue"></input>
        <label>Choose Image</label>
        <input type="file" onChange={handleChange} className="form-control"></input>
        {
          filesToUpload.map((item, index) => (
            <div key={index} style={{ marginTop: '10px' }}>
              <img src={URL.createObjectURL(item)} width="200px"></img>
              <span title="remove image selection" style={{ marginLeft: '5px' }}>
                <FaTrashAlt onClick={() => removeImageSelection(index)} color='red'></FaTrashAlt>
              </span>
            </div>
          ))
        }
        {
          filesToPreview.map((item, index) => (
            <div key={index} style={{ marginTop: '10px' }}>
              <img src={`${IMG_URL}/${item}`} width="200px"></img>
              <span title="remove exsisting image" style={{ marginLeft: '5px' }}>
                <FaTrashAlt onClick={() => removeExistingImage(item, index)} color='red'></FaTrashAlt>
              </span>
            </div>
          ))
        }
        <hr />
        <SubmitButton
          isSubmitting={props.isSubmitting}
        >

        </SubmitButton>
      </form>
    </>

  )
}