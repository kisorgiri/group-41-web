import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { handleError } from '../../../services/errorHandler';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/notify';
import { Loader } from '../../common/loader/loader.component';
import { ProductForm } from '../productForm/productForm.component';


export const EditProduct = () => {
  const [product, setProduct] = useState({});
  const [isLoading, setIsLoading] = useState(false)
  const [isSubmitting, setIsSubmitting] = useState(false)

  let params = useParams();
  let navigate = useNavigate();

  useEffect(() => {
    setIsLoading(true)
    httpClient.GET(`/products/${params.id}`, true)
      .then(({ data }) => {
        setProduct(data)
      })
      .catch(err => handleError(err))
      .finally(() => {
        setIsLoading(false)
      })
  }, [params.id])


  const edit = (data, files) => {
    setIsSubmitting(true)
    httpClient.UPLOAD('PUT', `/products/${params.id}`, data, files)
      .then(response => {
        notify.showInfo("Proudct Updated Successfully");
        navigate('/view_products')
      })
      .catch(err => {
        setIsSubmitting(false);
        handleError(err)
      })
  }

  let content = isLoading
    ? <Loader></Loader>
    : <ProductForm
      title="Update Product"
      submitCallback={edit}
      productData={product}
      isSubmitting={isSubmitting}
    ></ProductForm>
  return content;
}