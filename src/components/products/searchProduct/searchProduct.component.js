import { useEffect, useState } from "react";
import { handleError } from "../../../services/errorHandler";
import { httpClient } from "../../../utils/httpClient";
import { notify } from "../../../utils/notify";
import { SubmitButton } from "../../common/submitButton/submitButton.component";
import { ViewProducts } from "../viewProducts/viewProducts.component";


const defaultState = {
  name: '',
  category: '',
  minPrice: '',
  maxPrice: '',
  fromDate: '',
  toDate: '',
  tags: '',
  multipleDateRange: false
}


export const SearchProduct = () => {

  const [formFields, setFormFields] = useState({
    ...defaultState
  });
  const [categories, setCategories] = useState([]);
  const [allProducts, setAllProducts] = useState([]);
  const [searchResults, setSearchResults] = useState([]);
  const [names, setNames] = useState([]);
  const [isSubmitting, setIsSubmitting] = useState(false);

  useEffect(() => {
    httpClient.POST('/products/search', {})
      .then(({ data }) => {
        setAllProducts(data);
      })
      .catch(err => handleError(err));

    httpClient.POST('/category/search', {})
      .then(({ data }) => {
        setCategories(data);
      })
      .catch(err => handleError(err))
  }, [])

  const submit = e => {
    e.preventDefault();
    setIsSubmitting(true);
    const requestData = formFields;
    console.log('requestData ', requestData)
    if (!requestData.multipleDateRange) {
      requestData.toDate = requestData.fromDate
    }
    httpClient.POST('products/search', requestData)
      .then(({ data }) => {
        if (!data.length) {
          return notify.showInfo("No any products matched your search query");
        }
        setSearchResults(data);
      })
      .catch(err => {
        handleError(err);
      })
      .finally(() => {
        setIsSubmitting(false);

      })
  }

  const handleChange = e => {

    let { name, value, type, checked } = e.target;
    if (type === 'checkbox') {
      value = checked;
    }
    if (name === 'category') {
      filterNameAccordingToSelectedCategory(value);
    }

    setFormFields({
      ...formFields,
      [name]: value
    })

  }


  const filterNameAccordingToSelectedCategory = (selectedCategoryId) => {
    let names = allProducts.filter(item => item.category && (item.category._id === selectedCategoryId));
    setNames(names)
  }

  const reset = () => {
    setFormFields({ ...defaultState })
    setSearchResults([]);
  }

  let content = searchResults && searchResults.length > 0
    ? <ViewProducts
      productData={searchResults}
      resetSearch={reset}
    ></ViewProducts>
    : <>
      <h2>Search Product</h2>
      <p>Search for Products</p>
      <form onSubmit={submit} className="form-group">
        <label>Category</label>
        <select className="form-control" value={formFields.category} onChange={handleChange} name="category">
          <option value="">(Select Category)</option>
          {categories.map((cat) => (
            <option key={cat._id} value={cat._id}> {cat.name}</option>))
          }

        </select>
        {
          names && names.length > 0 && (
            <>
              <label>Name</label>
              <select className="form-control" value={formFields.name} onChange={handleChange} name="name">
                <option value="">(Select Category)</option>
                {names.map((item, i) => (
                  <option key={i} value={item.name}> {item.name}</option>))
                }
              </select>
            </>
          )
        }
        <label>Min Price</label>
        <input type="number" name="minPrice" onChange={handleChange} className="form-control" ></input>
        <label>Max Price</label>
        <input type="number" name="maxPrice" onChange={handleChange} className="form-control" ></input>

        <label>Select Date</label>
        <input type="date" name="fromDate" onChange={handleChange} className="form-control" ></input>
        <input type="checkbox" name="multipleDateRange" onChange={handleChange} />
        <label>Multiple Date Range</label>
        {
          formFields.multipleDateRange && (
            <>
              <br />
              <label>To Date</label>
              <input type="date" name="toDate" onChange={handleChange} className="form-control" ></input>
            </>
          )
        }
        <br />
        <label>Tags</label>
        <input type="text" onChange={handleChange} className="form-control" name="tags" placeholder="Tags"></input>
        <hr />
        <SubmitButton
          isSubmitting={isSubmitting}
        >

        </SubmitButton>
      </form>
    </>
  return content;
}