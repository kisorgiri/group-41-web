import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { handleError } from "../../../services/errorHandler"
import { httpClient } from "../../../utils/httpClient"
import { FaPencilAlt, FaTrashAlt } from 'react-icons/fa'
import { Loader } from "../../common/loader/loader.component"
import { notify } from "../../../utils/notify"
import { formatDate, relativeTime } from "../../../utils"
import { connect } from 'react-redux'
import { fetch_product_ac } from './../../../actions/product.ac'

const IMG_URL = process.env.REACT_APP_IMG_URL;

const ViewProductsComponent = (props) => {

  const [products, setProducts] = useState([])

  useEffect(() => {
    console.log('props >', props)
    if (props.productData) {
      return setProducts(props.productData)
    }
    props.fetch();


  }, [])

  const removeProduct = (id, index) => {
    // Always ask for confirmation
    let confirmation = window.confirm('Are you sure to remove?')
    if (confirmation) {
      httpClient.DELETE(`/products/${id}`, true)
        .then(response => {
          notify.showInfo("Product Removed")
          products.splice(index, 1);
          setProducts([...products]);
        })
        .catch(err => {
          handleError(err)
        })
    }
  }


  const reset = () => {
    props.resetSearch();
  }

  let content = props.isLoading
    ? <Loader></Loader>
    : <>
      <h2>View Products</h2>
      {
        props.resetSearch && (
          <button
            className="btn btn-success"
            onClick={reset}
          >
            Reset
          </button>
        )
      }

      <table className="table table-bordered">
        <thead>
          <tr>
            <th>S.N</th>
            <th>Name</th>
            <th>Category</th>
            <th>Tags</th>
            <th>Price</th>
            <th>Created At</th>
            <th>Last Updated</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {props.products.map((product, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td><Link to={`/product_details/${product._id}`}> {product.name}</Link> </td>
              <td>{product.category?.name}</td>
              <td>{product.tags.join(',')}</td>
              <td>{product.price}</td>
              <td>{formatDate(product.createdAt, 'ddd YYYY/MM/DD')}</td>
              <td>{relativeTime(product.updatedAt, 'minute')}</td>
              <td>
                <img src={`${IMG_URL}/${product.images[0]}`} width="180px" alt="product-image.png"></img>
              </td>
              <td>
                <Link to={`/edit_product/${product._id}`}>
                  <FaPencilAlt color="blue"></FaPencilAlt>
                </Link>
                |
                <FaTrashAlt onClick={() => removeProduct(product._id, index)} color="red"></FaTrashAlt>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </>

  return content;
}

// mapStateTOProps
// incoming data for a component inside props
const mapStateTOProps = (rootStore) => ({
  a: 'b',
  isLoading: rootStore.products.isLoading,
  products: rootStore.products.products,
})


// mapDispatchToProps
// outgoing events from components
const mapDispatchToProps = {
  fetch: fetch_product_ac
}

export const ViewProducts = connect(mapStateTOProps, mapDispatchToProps)(ViewProductsComponent)