import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { handleError } from '../../../services/errorHandler';
import { httpClient } from '../../../utils/httpClient';
import { notify } from '../../../utils/notify';
import { ProductForm } from '../productForm/productForm.component';


export const AddProduct = () => {

  const [isSubmitting, setIsSubmitting] = useState(false)

  let navigate = useNavigate();

  const add = (data, files) => {
    setIsSubmitting(true);
    httpClient.UPLOAD('POST', '/products', data, files)
      .then(response => {
        notify.showSuccess('Product Added Successfully');
        navigate('/view_products')
      })
      .catch(err => {
        handleError(err)
        setIsSubmitting(false)
      });
  }
  return (
    <ProductForm
      title="Add Product"
      description="Please Add required fields"
      submitCallback={add}
      isSubmitting={isSubmitting}
    ></ProductForm>
  )
}