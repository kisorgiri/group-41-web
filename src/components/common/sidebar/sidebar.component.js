import React from 'react';
import { Link } from 'react-router-dom';

import './sidebar.component.css';

export const Sidebar = (porps) => {
  return porps.isLoggedIn
    ? (
      <div className="sidebar">
        <Link to="/">Home</Link>
        <Link to="/add_product">Add Product</Link>
        <Link to="/view_products">View Products</Link>
        <Link to="/search_products">Search Products</Link>
        <hr />
        <Link to="/messages">Messages</Link>
        <Link to="/notifications">Notifications</Link>

      </div>
    )
    : null
}