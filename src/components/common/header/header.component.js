// stateless component

import React from 'react';
import { Link, useNavigate } from 'react-router-dom';
import './header.component.css';

export const Header = (props) => {
  let navigate = useNavigate();

  const logout = () => {
    // navigate to login
    localStorage.clear();

    navigate('/')
  }

  const getUserName = () => {
    let user = JSON.parse(localStorage.getItem('user'));
    return user.username;
  }

  let content = props.isLoggedIn
    ? <ul className="header_list">
      <li className="header_item">
        <Link to="/dashboard">Dashboard</Link>
      </li>
      <li className="header_item">
        <Link to="/about">About</Link>
      </li>
      <li className="header_item">
        <Link to="/contact">Contact</Link>
      </li>
      <li className="header_item">
        <Link to="/settings"> Settings</Link>
      </li>
      <li className="header_item">
        <button onClick={logout} className="btn btn-success logout">Logout</button>
        <span style={{ float: 'right', marginRight: '5px', marginTop: '5px', fontWeight: 'bold', }}>{getUserName()}</span>
      </li>
    </ul>
    : <ul className="header_list">
      <li className="header_item">
        <Link to="/home">Home</Link>

      </li>
      <li className="header_item">
        <Link to="/">Login</Link>

      </li>
      <li className="header_item">
        <Link to="/register">Register</Link>

      </li>
    </ul>
  return (
    <div className="header">
      {content}
    </div>
  )
}