import * as io from 'socket.io-client';

import React, { Component } from 'react'

export default class MessageComponent extends Component {

  constructor() {
    super();
  }

  componentDidMount() {
    this.socket = io.connect(process.env.REACT_APP_BASE_SOCKET_URL);
    // connection should be initilized
    this.runSocket();
  }

  runSocket() {
    this.socket.on('hello', (data) => {
      console.log('data in hello event', data)
      this.socket.emit('hi', 'hi from client')
    })
  }

  render() {
    return (
      <div>Message here</div>
    )
  }
}
