// statefull component with hooks

// state hooks

import React, { useState, useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import { handleError } from "../../../services/errorHandler";
import { httpClient } from "../../../utils/httpClient";
import { notify } from "../../../utils/notify";
import { SubmitButton } from "../../common/submitButton/submitButton.component";

const defaultState = {
  password: '',
  confirmPassword: ''
}

export const ResetPassword = (props) => {
  let navigate = useNavigate();

  const [isSubmitting, setIsSubmitting] = useState(false)
  const [isValidForm, setIsValidForm] = useState(false);
  const [formFields, setFormFields] = useState({
    ...defaultState
  })
  let params = useParams();

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormFields({
      ...formFields,
      [name]: value
    })
  }

  const handleSubmit = e => {
    e.preventDefault();
    setIsSubmitting(true)

    httpClient
      .POST(`/auth/reset-password/${params.id}`, formFields)
      .then(response => {
        notify.showSuccess("Password reset successfull please login");
        navigate('/');
      })
      .catch(err => {
        setIsSubmitting(false)
        handleError(err)
      })
  }


  return (
    <div className="auth_box">
      <h2>Reset Password</h2>
      <p>Please Choose your password wisely</p>
      <form className="form-group" onSubmit={handleSubmit}>
        <label>Password</label>
        <input type="password" placeholder="Password" name="password" className="form-control" onChange={handleChange} />
        <label>Confirm password</label>
        <input type="password" placeholder="Confirm password" name="confirmPassword" className="form-control" onChange={handleChange} />
        {/* typed value must be save in state */}
        <hr />
        <SubmitButton
          // is submitting in state
          // validForm state
          isSubmitting={isSubmitting}
        >
        </SubmitButton>

      </form>
      <p>Back to <Link to="/">Login</Link></p>
    </div>
  )
}