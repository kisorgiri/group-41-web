// statefull component with hooks

// state hooks

import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { handleError } from "../../../services/errorHandler";
import { httpClient } from "../../../utils/httpClient";
import { notify } from "../../../utils/notify";
import { SubmitButton } from "../../common/submitButton/submitButton.component";

const defaultState = {
  email: '',
}

export const ForgotPassword = (props) => {
  let navigate = useNavigate();

  const [isSubmitting, setIsSubmitting] = useState(false)
  const [isValidForm, setIsValidForm] = useState(false);
  const [email, setEmail] = useState('');
  const [formFields, setFormFields] = useState({
    ...defaultState
  })

  // useEffect(()=>{
  //   console.log('hi i am a effect block')
  // },[]) // componentDIdMount

  // useEffect(() => {
  //   console.log('once and in change of is submitting')
  //   // console.log('isSUbmitting now >>', isSubmitting)
  //   console.log('form Fields >>', formFields)
  //   // TODO use previous props or state
  // }, [formFields])

  // effect with cleanup

  useEffect(() => {
    return () => {
      // once component is destroyed
      console.log('component clean up block')
    }
  }, [])

  const handleChange = (e) => {
    const { name, value } = e.target;
    // setEmail(value);

    setFormFields({
      ...formFields,
      [name]: value
    })
  }

  const handleSubmit = e => {
    e.preventDefault();
    setIsSubmitting(true)

    // navigate({
    //   pathname: '/about',
    //   state: { name: 'kishor' },
    //   xyz: 'hello'
    // })

    // TODO:// send data during navigation
    // navigate('/', {
    //   state: { name: 'kishor' },
    // })
    httpClient
      .POST('/auth/forgot-password', formFields)
      .then(response => {
        notify.showSuccess("Password reset link sent to your email please check inbox");
        navigate('/', {
          state: { name: 'kishor' },
        })
      })
      .catch(err => {
        setIsSubmitting(false)
        handleError(err)
      })
  }


  return (
    <div className="auth_box">
      <h2>Forgot Password</h2>
      <p>Please provide your registered email address</p>
      <form className="form-group" onSubmit={handleSubmit}>
        <label>Email</label>
        <input type="text" placeholder="Email Address" name="email" className="form-control" onChange={handleChange} />
        {/* typed value must be save in state */}
        <hr />
        <SubmitButton
          // is submitting in state
          // validForm state
          isSubmitting={isSubmitting}
        >
        </SubmitButton>

      </form>
      <p>Back to <Link to="/">Login</Link></p>
    </div>
  )
}

// state implemetation in functional component are done using useState hooks

// effect Hooks ==> life cycle stages 

// effect hooks can be implemented with useEffect function call
// syntax varries according to its usage
// usage as
// componentDidMount (INIT)

// componentDidUpdate (UPDATE)

// componentWillUnMount( Destroy)


// routing 
// how an application behaves in change with routes
// url==> last parameters