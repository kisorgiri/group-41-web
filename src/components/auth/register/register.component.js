import React, { Component } from 'react';
import { SubmitButton } from '../../common/submitButton/submitButton.component';
import { Link, useNavigate } from 'react-router-dom';
import { notify } from '../../../utils/notify';
import { handleError } from '../../../services/errorHandler';
import { httpClient } from '../../../utils/httpClient';


const specialCharacters = ['!', '@', '#', '$', '%', '^', '&', '*']

const defaultForm = {
  name: '',
  username: '',
  password: '',
  confirmPassword: '',
  email: '',
  contactNumber: '',
  gender: '',
  temporaryAddress: '',
  permanentAddress: '',
  dob: ''
}

class RegisterComponent extends Component {
  constructor() {
    super();
    this.state = {
      data: {
        ...defaultForm
      },
      error: {
        ...defaultForm
      },
      isSubmitting: false,
      isValidForm: false
    }
    console.log('at first constructor')
  }

  componentDidMount() {
    // at init
    console.log('once component is fully rendered')
    // data prepration (API call )

  }

  componentDidUpdate(prevProps, prevState) {
    console.log('once component is updated', prevState.data.username);
    console.log('component updated with new state ', this.state.data.username)
    // check difference and take necessary action
  }

  componentWillUnmount() {
    console.log('once component is destroyed')
    // memory leakage management
    // stop all the async task and subsription
  }

  handleSubmit = e => {
    e.preventDefault();
    // http call with data
    httpClient
      .POST(`/auth/register`, this.state.data)
      .then(response => {
        notify.showSuccess("Registration successfull please login")
        this.props.navigate('/');
      })
      .catch(err => {
        handleError(err);
        this.setState({
          isSubmitting: false
        })
      })
  }

  handleChange = e => {

    let { name, value, type } = e.target;
    // this.setState((prevState) => {
    //   // should return updated state
    //   return {
    //     data: {
    //       ...prevState.data,
    //       [name]: value
    //     }
    //   }
    // })
    this.setState(prevState => ({
      data: {
        ...prevState.data,
        [name]: value
      }
    }), () => {
      // form validation
      this.validateForm(name);
    })
  }

  validateForm = (filedName) => {
    const { data } = this.state;
    let errMsg;
    switch (filedName) {
      case 'username':
        errMsg = data[filedName]
          ? data[filedName].length > 6
            ? ''
            : 'username must be 6 characters long'
          : 'required field*'
        break;
      case 'password':
        errMsg = data[filedName]
          ? data['confirmPassword']
            ? data['confirmPassword'] === data[filedName]
              ? ''
              : 'password didnot match'
            : data[filedName].length > 8
              ? this.checkSpecialCharacterExistance(data[filedName])
                ? ''
                : 'password must have one special characters'
              : 'Weak Password'
          : 'required field*'
        break;

      case 'confirmPassword':
        errMsg = data[filedName]
          ? data['password']
            ? data['password'] === data[filedName]
              ? ''
              : 'password did not match'
            : data[filedName].length > 8
              ? this.checkSpecialCharacterExistance(data[filedName])
                ? ''
                : 'password must have one special characters'
              : 'password must be 8 characters long'
          : 'requried field*';
        break;

      case 'email':
        errMsg = data[filedName]
          ? data[filedName].includes('@') && data[filedName].includes('.com')
            ? ''
            : 'invalid email'
          : 'required field*'
        break;
      default:
        break;
    }

    this.setState(prevState => ({
      error: {
        ...prevState.error,
        [filedName]: errMsg
      }
    }), () => {
      const { error } = this.state;
      const errors = Object
        .values(error)
        .filter(err => err);

      this.setState({
        isValidForm: errors.length === 0
      })
    })

  }

  checkSpecialCharacterExistance = (str) => {
    // array
    // string
    // todo ==> check string has one of the element from array
    // loop ==> 
    let hasSpecialCharacter = specialCharacters.some((char, index) => str.includes(char));
    return hasSpecialCharacter;
  }

  render() {
    console.log('render at 2nd')
    const { error } = this.state;
    return (
      <div className="auth_box">
        <h2>Register</h2>
        <p>Please Register to continue</p>
        <form className="form-group" onSubmit={this.handleSubmit}>
          <label>Name</label>
          <input type="text" name="name" placeholder="Name" className="form-control" onChange={this.handleChange}></input>
          <label>Username</label>
          <input type="text" name="username" placeholder="Username" className="form-control" onChange={this.handleChange}></input>
          <p className="error">{error.username}</p>
          <label>Password</label>
          <input type="password" name="password" placeholder="Password" className="form-control" onChange={this.handleChange}></input>
          <p className="error">{error.password}</p>
          <label>Confirm Password</label>
          <input type="password" name="confirmPassword" placeholder="Confirm Password" className="form-control" onChange={this.handleChange}></input>
          <p className="error">{error.confirmPassword}</p>
          <label>Email</label>
          <input type="text" name="email" placeholder="Email" className="form-control" onChange={this.handleChange}></input>
          <p className="error">{error.email}</p>

          <label>Contact Number</label>
          <input type="number" name="contactNumber" className="form-control" onChange={this.handleChange}></input>
          <label>D.O.B</label>
          <input type="date" name="dob" className="form-control" onChange={this.handleChange}></input>
          <label>Gender</label>
          <br />
          <input type="radio" name="gender" value="male" onChange={this.handleChange} />
          <label>
            Male&nbsp;
          </label>
          <input type="radio" name="gender" value="female" onChange={this.handleChange} />
          <label>
            Female &nbsp;
          </label>
          <label>
            <input type="radio" name="gender" value="others" onChange={this.handleChange} />
            Others &nbsp;
          </label>
          <br />
          <label>Permanent Address</label>
          <input type="text" name="permanentAddress" placeholder="Permanent Address" className="form-control" onChange={this.handleChange}></input>
          <label>Temporary Address</label>
          <input type="text" name="temporaryAddress" placeholder="Temporary Address" className="form-control" onChange={this.handleChange}></input>
          <hr />
          <SubmitButton
            isSubmitting={this.state.isSubmitting}
            isDisabled={!this.state.isValidForm}
          >

          </SubmitButton>
        </form>
        <p>Already registered?</p>
        <p>back to <Link to="/">login</Link></p>
      </div>
    )
  }
}

// main lesson when using external (thirdparty) utility library
// try to minimize its scope within few file

export const Register = (props) => {
  let navigate = useNavigate();

  return (
    <RegisterComponent navigate={navigate} />
  )
}