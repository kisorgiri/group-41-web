import React, { Component, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { SubmitButton } from '../../common/submitButton/submitButton.component';
import { notify } from '../../../utils/notify';
import { handleError } from '../../../services/errorHandler';
import { httpClient } from '../../../utils/httpClient';

const defaultState = {
  username: '',
  password: ''
}
class LoginComponent extends Component {

  constructor() {
    super();

    this.state = {
      data: {
        ...defaultState
      },
      error: {
        ...defaultState
      },
      remember_me: false,
      isSubmitting: false,
    }; // component data
  }

  // componentDidMount() {
  //   const isRemember = JSON.parse(localStorage.getItem('remember_me'))
  //   console.log('isRemeber >>', isRemember)
  //   console.log('type of >>', typeof (isRemember))
  //   if (isRemember) {
  //     console.log('now redirect')
  //     // navigate 
  //     // we need hooks 
  //     this.props.navigate('/dashboard')
  //   }
  // }

  submit = (e) => {
    e.preventDefault();
    const isValid = this.validateForm();
    if (!isValid) return;

    this.setState({
      isSubmitting: true
    })

    httpClient.POST(`/auth/login`, this.state.data)
      .then(response => {
        notify.showSuccess(`Welcome ${response.data.user.username}`)

        // localstorage setup
        localStorage.setItem('token', response.data.token);
        localStorage.setItem('user', JSON.stringify(response.data.user));
        localStorage.setItem('remember_me', this.state.remember_me)

        this.props.navigate('/dashboard')
      })
      .catch(err => {
        handleError(err);
        this.setState({
          isSubmitting: false
        })
      })
    // API call garne 
    // data pathaune for server
    // data extractraction from state
  }

  handleChange = (e) => {
    let { name, value, type, checked } = e.target;
    // to update state we will use setState method
    // this.state.username= value
    console.log('this >>', checked)
    if (type === 'checkbox') {
      return this.setState({
        [name]: checked
      })
      // if remember_me is inside data
      // value = checked;
    }

    this.setState(preState => ({
      data: {
        ...preState.data,
        [name]: value
      }
    }), () => {
      if (this.state.error[name]) {
        this.validateForm();
      }
    })
    // async
    // console.log('this.state >', this.state)
  }


  validateForm = () => {
    let usernameErr;
    let passwordErr;
    let validForm = true;

    const { data } = this.state;
    if (!data.username) {
      usernameErr = 'required field*';
      validForm = false;
    }

    if (!data.password) {
      passwordErr = 'required field*';
      validForm = false;
    }

    // async
    this.setState({
      error: {
        username: usernameErr,
        password: passwordErr
      }
    })

    return validForm;

  }


  // each time components data (props or state) is updated render block is called
  render() {
    // keep UI logic inside render
    // it is responsible for returning a single html node

    return (
      <div className="auth_box">
        <h2>Login</h2>
        <p>Please Login to start your session</p>
        <form className="form-group" onSubmit={this.submit} noValidate>
          <label htmlFor="uname">Username</label>
          <input className="form-control is-invalid" value={this.state.username} type="text" id="uname" name="username" placeholder="Username" onChange={this.handleChange}></input>
          <p className="error">{this.state.error.username}</p>
          <label htmlFor="pwd">Password</label>
          <input className="form-control" type="password" id="pwd" name="password" placeholder="Password" onChange={this.handleChange}></input>
          <p className="error">{this.state.error.password}</p>
          <input type="checkbox" name="remember_me" onChange={this.handleChange}></input>
          <label> &nbsp;Remember Me</label>
          <hr />
          <SubmitButton
            enabledLabel="Login"
            isSubmitting={this.state.isSubmitting}
          ></SubmitButton>
        </form>
        <p>Don't have an Account?</p>
        <p style={{ float: 'left' }}>Register <Link to="/register">here</Link></p>
        <p style={{ float: 'right' }}><Link to="/forgot_password">forgot password?</Link></p>
      </div>
    )
  }
}

// note anchor tag(<a> ) should be replaced by <link> to prevent page reloading

export const Login = (props) => {
  const navigate = useNavigate();
  let location = useLocation();
  console.log('location', location)
  useEffect(() => {
    const isRemember = JSON.parse(localStorage.getItem('remember_me'))
    if (isRemember) {
      // navigate 
      // we need hooks 
      navigate('/dashboard')
    }
  }, [])

  return (
    <LoginComponent navigate={navigate} />
  )
}